<?php
$programmationDates = [
    '2018-10-10',
    '2018-10-12',
    '2018-10-13',
    '2018-10-14',
    '2018-10-18',
    '2018-10-20',
    '2018-10-22',
    '2018-10-23',
  ];
?>
<html>
<head>
    <meta charset="utf-8">
    <title>Cinemas Novius</title>
    <link rel="stylesheet" type="text/css" href="style/style_index.css" media="all"/>
    <script src="javascript/js_index.js"></script>
    <script src="javascript/carousel.js" async></script> 
</head>

<header>
    <div style="float:left;" class="img">
        <img src="assets/img/logo.png" width="105%" height="12%" alt="Problèmes d'affichage" border="0" />
    </div>
    <div style="float:left;" class="type2 cine">
        <a><i><b>cinéma 1</a><br><br>
        <a>cinéma 2</a><br><br>
        <a>cinéma 3</b></i></a><br><br>
    </div>
    <div style="float:left;" class="type1 cine">
        <a>PROGRAMME PDF
        <img src="assets/img/download.png" width="14%" height="3%" alt="Problèmes d'affichage" border="0" />
</a>
    </div>
    <div style="float:left;margin-top:0.8%;" class="type1 titre1">
        <a>Nos Cinémas</a>
    </div>
    <div style="float:left;" class="type2">
        <p>-</p>
    </div>
    <div style="float:left;margin-top:0.8%;" class="type2 titre1">
        <a>Scolaires</a>
    </div>
    <div style="float:left;" class="type2">
        <p>-</p>
    </div>
    <div style="float:left; margin-top:0.8%;" class="type2 titre1">
        <a>Contact</a>
    </div>
    <div style="float:left;" class="type2">
        <p>-</p>
    </div>
    <div style="float:left;margin-top:0.8%;" class="type2 titre1">
        <a>Infos pratiques</a>
    </div>
    <div style="float:left;" class="type1 rs">
    <a>twitter</a>
    </div>
    <div style="float:left;" class="type2 rs">
    <a>facebook</a>
    </div>
    <div style="float:left;" class="billeterie">
        <ul id="nav">
      <li><a> BILLETERIE </a>
         <ul class="menu_deroulant">
            <li><a href="#nogo">cinéma 1</a></li><br>
            <li><a href="#nogo">cinéma 2</a></li><br>
            <li><a href="#nogo">cinéma 3</a></li><br>
         </ul>
      </li>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div style="float:left;" class="onglet">
    <ul id="nav">
      <li><a><b> PROGRAMMATION </b></a>
         <ul class="menu_deroulant">
            <li><a href="#nogo"><i>.|</i> cinéma 1</a></li><br>
            <li><a href="#nogo"><i>.|</i> cinéma 2</a></li><br>
            <li><a href="#nogo"><i>.|</i> cinéma 3</a></li><br>
            <li><a href="#nogo"><i>.|</i> calendrier général</a></li><br>
         </ul>
      </li>
    </div>
    <div style="float:left;" class="">
        <p>-</p>
    </div>
    <div style="float:left;" class="onglet">
    <ul id="nav">
      <li><a><b> EVENEMENT </b></a>
      </li>
    </div>
    <div style="float:left;" class="">
        <p>-</p>
    </div>
    <div style="float:left;" class=" onglet">
    <ul id="nav">
      <li><a><b> AVANT-PREMIÈRES </b></a>
    </div>
    <div style="float:left;" class="">
        <p>-</p>
    </div>
    <div style="float:left;" class=" onglet">
    <ul id="nav">
      <li><a><b> RENDEZ-VOUS </b></a>
      </li>
    </div>
    <div style="float:left;" class="">
        <p>-</p>
    </div>
    <div style="float:left;" class=" onglet">
    <ul id="nav">
      <li><a><b> SORTIES NATIONALES </b></a>
      </li>
    </div>
    
</header>

<body>
    <div class="img_background" style="width:50%; float:left; display: inline-block;">
        <p><img src="assets/img/background.png" width="110%" height="70%" alt="Problèmes d'affichage" border="0" /></p>
    </div>
    <div style="float:left;display: inline-block;" class="background_body">
        <p>SORTIE<br>NATIONALE</p>
        <p>Don't Worry,<br>He Won't Get Far On Foot</p>
        <p>de Gus Van Sant<br>2018</p>
    </div>
    <div style="width:50%; float:left;" class="body_footer" id="carousel">
        <p>Programmation</p>
        <?php
            foreach($programmationDates as $value){
                echo ('<div class="div_carousel">').$value.('</div>');
            }
        ?>
    </div>
</body>

</html>